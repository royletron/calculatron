const express = require("express");

const app = express();

app.get("/addition/:a/:b", (req, res) =>
  res.status(200).send({ answer: 99, followOn: "" })
);

app.get("/subtraction/:a/:b", (req, res) =>
  res.status(200).send({ answer: 99, followOn: "" })
);

module.exports = app;
